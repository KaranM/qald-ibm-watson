import pdb
import sys
import xml.etree.ElementTree as ET

def get_answer_list(answers):
    '''get a list of answers from an answers node'''
    answer_list = []
    for answer in answers:
	for child in answer:
	    answer_list.append(child.text)
    return answer_list

def get_answers(question):
    '''return a tuple
    key is question id
    value is list of answers from the question node'''
    for child in question:
        if child.tag == 'answers':
            answer = get_answer_list(child)
    #avoid breaking program - always return something
    question_id = int(question.attrib['id'])
    return question_id, answer

#read the xml file from standard input
answers_xml = sys.stdin.read()
root = ET.fromstring(answers_xml)

answer_list = []

for question in root:
    answer_list.append(get_answers(question))

#sort the answers by id
answer_list.sort()
for answer in answer_list:
    print ','.join(answer[1])