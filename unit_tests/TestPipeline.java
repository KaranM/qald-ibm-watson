import static org.junit.Assert.*;

import org.junit.Test;


public class TestPipeline {


	@Test
	public void testVerifyDate() {
		assertTrue(Pipeline.verifyDate("1994-22-33"));
		assertFalse(Pipeline.verifyDate("23-24-1999"));
	}

}
