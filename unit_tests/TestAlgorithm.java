import static org.junit.Assert.*;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;

public class TestAlgorithm {

	@Test
	public void testAlgorithm() {
		ArrayList<String> dependencyList;
		ArrayList<String> results;
		
//		String[] dependencyArray = {"Brooklyn Bridge","crosses","river"};
//		dependencyList = new ArrayList<String>(Arrays.asList(dependencyArray));
//		results = Algorithm.answerQuestionFromDependencyList(dependencyList);
//		assertTrue(results.get(0).contains("East_River"));
//		
//		String[] obamaDependencyArray = {"Barack Obama","wife","graduate","university"};
//		dependencyList = new ArrayList<String>(Arrays.asList(obamaDependencyArray));
//		results = Algorithm.answerQuestionFromDependencyList(dependencyList);
//		assertTrue(results.get(0).contains("Princeton"));
		
		assertTrue(Algorithm.splitCamelCase("barackHusseinObama").equals("barack hussein obama"));
	}
	
//	@Test
//	public void testListAlgorithm() {
//		ArrayList<String> dependencyList;
//		ArrayList<String> results;
//		
//		String[] charmedDependencyList = {"Charmed","starring","birthday"};
//		dependencyList = new ArrayList<String>(Arrays.asList(charmedDependencyList));
//		results = Algorithm.answerListQuestion(dependencyList);
//		System.out.println("hi");
//	}
	
//	@Test
//	public void testCamelCase() {
//		
//		String x =DBPediaInterface.splitCamelCase("BarackObama");
//		assertTrue(x.equals("Barack Obama"));
//		
//	}

}
