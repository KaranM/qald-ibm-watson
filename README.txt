Dependencies:
    Java 1.7
    JUnit 4.11
    javax.json
    Stanford Core-NLP
    Weka
    Queries the DBPedia and JobimText APIs
    DBPedia server must not be under maintenance

Stanford CoreNLP generates NE tags, POS tags, lemmas, and syntactic dependencies of the questions in the
training and testing data. Our repo includes the final files generated but not the code needed to run and
parse the output of Stanford Core NLP. For those interested in how to generate the tags, refer to
http://nlp.stanford.edu/software/corenlp.shtml

To run main method:
    Main method is in Main.java.  It takes 6(!) command-line arguments.  They are:
        1) The path to the file with the test questions
        2) The path to the file with the output from the weka type classifier
        3) The path to the file with the output from the weka list classifier
        4) The output path
        5) The index of the first question you want to answer.
        6) 1 plus the index of the last question you want to answer.
    
    To run the program with the files already in the repo, run:
        java Main test_questions.txt typeClass.txt listClass.txt testAnswers.txt 0 98