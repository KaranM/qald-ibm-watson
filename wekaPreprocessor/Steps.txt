

1. Read the training questions manually labelled by Sam
2. Use the python script to convert these to the annotated questions 
3. Create arff files out of these using the java code CsvToArff
4. Create models in weka using these arff files :  a. ListModel b. TypeModel
5. Use the model to make predictions


http://weka.wikispaces.com/Making+predictions

//	java -cp /Users/karanmatnani/Downloads/weka-3-6-10/weka.jar weka.classifiers.trees.J48 -c 1 -t typeOut.arff -T testType.arff -p 1-18
java -cp /Users/karanmatnani/Downloads/weka-3-6-10/weka.jar weka.classifiers.trees.J48 -T Type.arff -l /Users/karanmatnani/Desktop/Watson/qald/wekaPreprocessor/TypeModel.model -c 1 -p 0

Types:
java -cp /Users/karanmatnani/Downloads/weka-3-6-10/weka.jar weka.filters.supervised.attribute.AddClassification -serialized /Users/karanmatnani/Desktop/Watson/qald/wekaPreprocessor/TypeModel.model -classification -i typeOut.arff -o typeOut_classified.arff -c 1

tail -n 99 typeOut_classified.arff | cut -d ',' -f19 > typeClass.txt


List:
java -cp /Users/karanmatnani/Downloads/weka-3-6-10/weka.jar weka.filters.supervised.attribute.AddClassification -serialized /Users/karanmatnani/Desktop/Watson/qald/wekaPreprocessor/ListModel.model -classification -i listOut.arff -o listOut_classified.arff -c 1

tail -n 99 ListOut_classified.arff | cut -d ',' -f19 > listClass.txt


6. Get the testing questions.
7. Use the new java create_test_attributes program to create its attributes
8. Create arff files out of these using the java code CsvToArff

We may have to modify the signature of the arff file manually.

9. Use the model to make predictions

java -cp /Users/karanmatnani/Downloads/weka-3-6-10/weka.jar weka.classifiers.trees.J48 -c 1 -T Type.arff -l /Users/karanmatnani/Desktop/Watson/qald/wekaPreprocessor/TypeModel.model -p 0 > predictions_Test_type.txt

java -cp /Users/karanmatnani/Desktop/Watson/qald/wekaPreprocessor/script/javaCode/jars/weka.jar weka.classifiers.trees.J48 -c 1 -T finalType.arff -l /Users/karanmatnani/Desktop/Watson/qald/wekaPreprocessor/script/TrainingModel/TypeModel.model -p 0 > predictions_Test_type.txt



tail -n 10 predictions_Test_type.txt > final_type

10. Run the final_type through the java program : Predictor to get a file with just the predictions