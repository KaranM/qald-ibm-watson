import csv

'''script to create attributes for the training data for question types'''
attributes = ['who','what','when','where','which','how','city','state','country','?','day',\
                    'is', 'people', 'person', 'president', 'how many', 'give me all', 'was', 'did']
def main():
    with open('type.csv','w') as type_output_file:
      with open('list.csv','w') as list_output_file:
        with open('train_question_types_manually_annotated.csv','r') as input_file:
            csvreader = csv.reader(input_file, delimiter=',', quotechar='"')
            firstline = True
            for row in csvreader:
                if firstline:
		   print row[0]
                   type_output_file.write(row[1] + ',' + ','.join(attributes) + '\n')
                   list_output_file.write(row[2] + ',' + ','.join(attributes) + '\n')
                   firstline = False
                else:
                    results = []
                    row[0] = row[0].lower()
                    
                    #append features one by one
                    results.append('yes' if 'who' in row[0] else 'no')
                    results.append('yes' if 'what' in row[0] else 'no')
                    results.append('yes' if 'when' in row[0] else 'no')
                    results.append('yes' if 'where' in row[0] else 'no')
                    results.append('yes' if 'which' in row[0] else 'no')
                    results.append('yes' if 'how' in row[0] else 'no')
                    results.append('yes' if 'city' in row[0] or 'cities' in row[0] else 'no')
                    results.append('yes' if 'state' in row[0] or 'states' in row[0] else 'no')
                    results.append('yes' if 'country' in row[0] or 'countries' in row[0] else 'no')
                    results.append('yes' if row[0][-1] == '?' else 'no')
                    results.append('yes' if 'day' in row[0] or 'days' in row[0] else 'no')
                    results.append('yes' if row[0][0:2] == 'is' else 'no')
                    results.append('yes' if 'people' in row[0] else 'no')
                    results.append('yes' if 'person' in row[0] else 'no')
                    results.append('yes' if 'president' in row[0] or 'presidents' in row[0] else 'no')
                    results.append('yes' if row[0][:8] == 'how many' else 'no')
                    
                    #just discovered startswith method, too lazy to change other rows
                    results.append('yes' if row[0].startswith('give me all') else 'no')
                    results.append('yes' if row[0].startswith('was') else 'no')
                    results.append('yes' if row[0].startswith('did') else 'no')
                    #print the results to the new file
                    row.extend(results)
                    type_output_file.write(row[1] + ',' + ','.join(row[3:]) + '\n')
                    list_output_file.write(row[2] + ',' + ','.join(row[3:]) + '\n')
#		    type_output_file.write("%s%s" % (results,"\n"))
 #	                   list_output_file.write("%s%s" % (results,"\n"))

	print(results)
if __name__ == '__main__':
    main()
