package wekaPreprocessor;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;

public class Predictor {

	public static void main(String[] args) {
		
		Predictor p = new Predictor();
		
		p.typePredict(args[0],args[1]);
		p.listPredict(args[2],args[3]);
		
	}
	
	
	public void listPredict(String ipFile, String opFile) {
		String[] attributes = {"yes","no"};
		try{
			PrintWriter pw = new PrintWriter(opFile);
			BufferedReader br = new BufferedReader(new FileReader(ipFile));
			String line = "";
			while((line=br.readLine())!=null){
				for(int i=0 ;i < attributes.length ; i++){
					if(line.contains(attributes[i])){
						pw.println(attributes[i]);
					}
				}
			}
			pw.close();
			br.close();
		} catch(IOException io){
		}
	}


	public void typePredict(String ipFile, String opFile){
		
		String[] attributes = {"person","object","date","number","boolean","place"};
		try{
			PrintWriter pw = new PrintWriter(opFile);
			BufferedReader br = new BufferedReader(new FileReader(ipFile));
			String line = "";
			while((line=br.readLine())!=null){
				for(int i=0 ;i < attributes.length ; i++){
					if(line.contains(attributes[i])){
						pw.println(attributes[i]);
					}
				}
			}
			pw.close();
			br.close();
		} catch(IOException io){
		}
	}
}
