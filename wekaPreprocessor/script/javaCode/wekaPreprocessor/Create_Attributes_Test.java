package wekaPreprocessor;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import au.com.bytecode.opencsv.CSVWriter;


/**
 *	1. Read in inputfile
 *	2. Parse line by line
 *	3. Take question, and check for all the attributes, if found, append yes, else no
 *	4. Write each line to an output file
 */
public class Create_Attributes_Test {

	public static void main(String[] args) {
		String[]	attributesType = {"Type","who","what","when","where","which","how","city","state","country","?","day","is", "people", "person", "president", "how many", "give me all", "was", "did"};
		String[]	attributesList = {"List","who","what","when","where","which","how","city","state","country","?","day","is", "people", "person", "president", "how many", "give me all", "was", "did"};


		// Replace this with the name of the file with the 

		String ipFile = args[0];

		String opType = args[1];
		String opList = args[2];
		try {
			CSVWriter writerType = new CSVWriter(new FileWriter(opType), ',', CSVWriter.NO_QUOTE_CHARACTER);
			CSVWriter writerList = new CSVWriter(new FileWriter(opList), ',', CSVWriter.NO_QUOTE_CHARACTER);
			writerType.writeNext(attributesType);
			writerList.writeNext(attributesList);
			@SuppressWarnings("resource")
			BufferedReader br = new BufferedReader(new FileReader(ipFile));
			String question = "";
			while((question = br.readLine())!=null){
				String [] s = new String[20];
				question = question.toLowerCase();
				for(int i = 1 ; i < attributesType.length ; i++){

					if(i==12){
						if(question.startsWith("is")){
							s[i]="yes";
						}else{
							s[i]="no";
						}
					}else if(i==16){
						if(question.startsWith("How many") || question.startsWith("how many")){
							s[i]="yes";
						}else{
							s[i]="no";
						}
						
					}else if(i==17){
							
						if(question.startsWith("Give me all") || question.startsWith("give me all")){
							s[i]="yes";
							//System.out.println(question);
							//System.out.println(attributesType[i]);
						}else{
							s[i]="no";
						}
						
					} else if(i==7){
						if(question.contains("city") || question.contains("cities")){
							s[i]="yes";
						}else{
							s[i]="no";
						}
						
					}else if(i==8){
						if(question.contains("state") || question.contains("states")){
							s[i]="yes";
						}else{
							s[i]="no";
						}
						
					}else if(i==9){
						if(question.contains("country") || question.contains("countries")){
							s[i]="yes";
						}else{
							s[i]="no";
						}
						
					}else if(i==11){
						if(question.contains("day") || question.contains("days")){
							s[i]="yes";
						}else{
							s[i]="no";
						}
						
					}else if(i==15){
						if(question.contains("president") || question.contains("presidents")){
							s[i]="yes";
						}else{
							s[i]="no";
						}
						
					}else if(i==18){
						if(question.startsWith("was")){
							s[i]="yes";
						}else{
							s[i]="no";
						}
						
					}else if(i==19){
						if(question.startsWith("did")){
							s[i]="yes";
						}else{
							s[i]="no";
						}
						
					}
					
					else{
						if(question.contains(attributesType[i])){
							s[i]="yes";
						}else{
							s[i]="no";
						}
					}
				}
				s[0]="?";
				writerType.writeNext(s);
				writerList.writeNext(s);
			}
			writerType.close();
			writerList.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch(IOException ie){
		} 
	}

}
