#!/bin/bash

# $1 is the first argument passed by user, which should be the testing file in xml format
perl convertToCSV.pl $1 > test.txt

#removing keywords
cat test.txt | cut -f1 -d ';' > test_questions.txt

#Creating the attributes for the set
cd javaCode
rm -rf *.class
javac -cp "/Users/karanmatnani/Desktop/Watson/qald/wekaPreprocessor/script/javaCode/jars/*:wekaPreprocessor/*" wekaPreprocessor/Create_Attributes_Test.java
java -cp "/Users/karanmatnani/Desktop/Watson/qald/wekaPreprocessor/script/javaCode/jars/*:wekaPreprocessor/:." wekaPreprocessor/Create_Attributes_Test ../test.txt ../typeTest.txt ../listTest.txt
#java -cp "/Users/karanmatnani/Desktop/Watson/qald/wekaPreprocessor/script/javaCode/jars/*:wekaPreprocessor/:." wekaPreprocessor/Create_Attributes_Test ../train_questions.txt ../typeTest.txt ../listTest.txt


#Creating the ARFF files out of the newly generated CSV files
javac -cp "/Users/karanmatnani/Desktop/Watson/qald/wekaPreprocessor/script/javaCode/jars/*:wekaPreprocessor/*" wekaPreprocessor/CsvToArff.java
java -cp "/Users/karanmatnani/Desktop/Watson/qald/wekaPreprocessor/script/javaCode/jars/*:wekaPreprocessor/:." wekaPreprocessor/CsvToArff ../typeTest.txt ../typeTest.arff
java -cp "/Users/karanmatnani/Desktop/Watson/qald/wekaPreprocessor/script/javaCode/jars/*:wekaPreprocessor/:." wekaPreprocessor/CsvToArff ../listTest.txt ../listTest.arff

cd ..
#Repairing the ARFF files
sed '1,22d' listTest.arff > temp.arff
head -n 22 TrainingModel/list.arff > finalList.arff
cat temp.arff >> finalList.arff
rm -f temp.arff

sed '1,22d' typeTest.arff > temp2.arff
head -n 22 TrainingModel/type.arff > finalType.arff
cat temp2.arff >> finalType.arff
rm -f temp2.arff

#Generating predictions using the model
java -cp /Users/karanmatnani/Desktop/Watson/qald/wekaPreprocessor/script/javaCode/jars/weka.jar weka.classifiers.trees.J48 -c 1 -T finalType.arff -l /Users/karanmatnani/Desktop/Watson/qald/wekaPreprocessor/script/TrainingModel/TypeModelFinal.model -p 0 > predictions_Test_type.txt

java -cp /Users/karanmatnani/Desktop/Watson/qald/wekaPreprocessor/script/javaCode/jars/weka.jar weka.classifiers.trees.J48 -c 1 -T finalList.arff -l /Users/karanmatnani/Desktop/Watson/qald/wekaPreprocessor/script/TrainingModel/ListModelFinal.model -p 0 > predictions_Test_list.txt

cd javaCode
# Stripping out the required parts of both files
javac -cp "/Users/karanmatnani/Desktop/Watson/qald/wekaPreprocessor/script/javaCode/jars/*:wekaPreprocessor/*" wekaPreprocessor/Predictor.java
java -cp "/Users/karanmatnani/Desktop/Watson/qald/wekaPreprocessor/script/javaCode/jars/*:wekaPreprocessor/:." wekaPreprocessor/Predictor ../predictions_Test_type.txt ../typeClass.txt ../predictions_Test_list.txt ../listClass.txt






