#!/bin/bash

# $1 is the first argument passed by user, which should be the testing file in xml format
perl convertToCSV.pl $1 > test.txt

#removing keywords
cat test.txt | cut -f1 -d ';' > test_questions.txt

#Creating the attributes for the set
cd javaCode
javac -cp "/Users/karanmatnani/Desktop/Watson/qald/wekaPreprocessor/javaCode/jars/*:wekaPreprocessor/*" wekaPreprocessor/Create_Attributes_Test.java
java -cp "/Users/karanmatnani/Desktop/Watson/qald/wekaPreprocessor/javaCode/jars/*:wekaPreprocessor/:." wekaPreprocessor/Create_Attributes_Test test_questions.txt typeTest.txt listTest.txt
cd ..
