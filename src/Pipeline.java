import java.util.ArrayList;
import java.util.regex.*;
import java.util.Scanner;
import java.io.File;
import java.io.IOException;

public class Pipeline {
	
	/**
	 * Create a list of questions given three files
	 * questionPath is a file with the raw questions
	 * typePath is the output from the weka model for types
	 * listPath is the output from the weka model for lists
	 * @param questionPath
	 * @param typePath
	 * @param listPath
	 * @return
	 */
	public static ArrayList<Question> createQuestionsFromFiles(String questionPath, 
			String typePath, String listPath) {
		ArrayList<Question> questionList = createQuestionsFromFile(questionPath);
		
//		//temporarily only do it for one
//		Question onlyQuestion = questionList.get(0);
//		questionList = new ArrayList<Question>();
//		questionList.add(onlyQuestion);
		
		//fill in question types before generating the PODs
		updateQuestionTypes(questionList, typePath);
		updateQuestionIsLists(questionList, listPath);
		
		updateQuestionPODs(questionList);
		
		return questionList;
	}
	
	public static ArrayList<String> answerQuestion(Question question) {
		if(question.isList) {
			return Algorithm.answerListQuestion(question.podLists.get(0));
		} else if (question.questionType == QuestionType.BOOLEAN) {
			return Algorithm.answerBinaryQuestion(question.podLists.get(0));
		} else {
			return Algorithm.answerQuestionFromDependencyList(question.podLists.get(0));
		}
	}
	
	private static boolean verifyType(String dbpediaResource, QuestionType type) {
		boolean correctType = false;
		if(type == QuestionType.PLACE) {
			correctType = verifyPlace(dbpediaResource);
		}
		if(type == QuestionType.DATE) {
			correctType = verifyDate(dbpediaResource);
		}
		
		return correctType;
	}
	
	private static boolean verifyPlace(String dbpediaResource) {
		String query = String.format("SELECT \"yes\" WHERE {dbpedia:%s rdf:type dbpedia-owl:Place}",
				DBPediaInterface.shortenResource(dbpediaResource));
		String placeResults = DBPediaInterface.queryDBPedia(query);
		int numResults = placeResults.split("\n").length - 1;
		if(numResults > 0) {
			return true;
		} else {
			return false;
		}
	}
	
	public static boolean verifyDate(String dbpediaResource) {
		//assume the date is given by the regex yyyy-mm-dd
		Pattern pattern = Pattern.compile("\\d{4}-\\d{2}-\\d{2}");
		Matcher matcher = pattern.matcher(dbpediaResource);
		if(matcher.find()) {
			return true;
		} else {
			return false;
		}
		
	}
	
	private static void updateQuestionTypes(ArrayList<Question> questionList, String typePath) {
		try {
			Scanner scanner = new Scanner(new File(typePath));
			//assume that the number of questions is equal to the number of lines in the type file
			//if not, bad things will happen
			for(Question question : questionList) {
				question.setType(scanner.nextLine());
			}
			
			scanner.close();
		} catch(IOException ioe) {
			System.err.println("Could not read question types from file.");
			ioe.printStackTrace();
			return;
		}
	}
	
	private static void updateQuestionIsLists(ArrayList<Question> questionList, String isListPath) {
		try {
			Scanner scanner = new Scanner(new File(isListPath));
			for(Question question : questionList) {
				question.setIsList(scanner.nextLine());
			}
			scanner.close();
		} catch(IOException ioe) {
			System.err.println("Could not read list file.");
			ioe.printStackTrace();
			return;
		}
	}
	
	private static ArrayList<Question> createQuestionsFromFile(String questionPath) {
		ArrayList<Question> questionList = new ArrayList<>();
		try {
			Scanner scanner = new Scanner(new File(questionPath));
			while(scanner.hasNextLine()) {
				String questionText = scanner.nextLine();
				if(questionText.length() > 0) {
					Question question = new Question();
					question.text = questionText;
					questionList.add(question);
				}
			}
			scanner.close();
		} catch(IOException ioe) {
			System.err.println("Error reading questions from file");
			ioe.printStackTrace();
		}
		return questionList;
	}
	
	private static void updateQuestionPODs(ArrayList<Question> questionList) {
		PodsGeneration.readCSV();
		for(Question question : questionList) {
			question.podLists.add(PodsGeneration.create_pods(question));
		}
	}
	
	public static String fixKeyword(String keyword) {
		if(keyword.charAt(0) == '"' && keyword.charAt(1) == ' ') {
			keyword = keyword.substring(2);
		}
		return keyword;
	}
	
	
}