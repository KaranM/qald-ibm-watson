import java.io.Serializable;

public class PODNode implements Serializable{
		String text; 
		String POSTag;
		String lemma; // lemmatized
		public PODNode()
		{
		}
		
		public PODNode(String t, String pos, String lem)
		{
			this.text=t;
			this.POSTag=pos;
			this.lemma=lem;
		}
}
