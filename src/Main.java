import java.io.File;
import java.io.FileWriter;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.io.IOException;

public class Main {
	
	static String QUESTIONS_FILE_PATH = "train_questions.txt";
	static String TYPE_FILE_PATH = "trainTypeClass.txt";
	static String LIST_FILE_PATH = "trainListClass.txt";
	static String OUTPUT_PATH = "trainAnswers.txt";

	
	/**
	 * Command line arguments:
	 * first argument: questions .txt file
	 * second argument: type file
	 * third argument: list file
	 * fourth argument: output path
	 * if there's a fifth argument: start number
	 * If there's a sixth argument: end number
	 * @param args
	 */
	public static void main(String[] args) { 
		
		int firstQuestion = 0;
		int lastQuestion = 99;
		
		if(args.length >= 4) {
			QUESTIONS_FILE_PATH = args[0];
			TYPE_FILE_PATH = args[1];
			LIST_FILE_PATH = args[2];
			OUTPUT_PATH = args[3];
		}
		if(args.length >= 5) {
			firstQuestion = Integer.parseInt(args[4]);
		}
		if(args.length >= 6) {
			lastQuestion = Integer.parseInt(args[5]);
		}
	
		
//		//this code was used to initially serialize the questions
		PodsGeneration.readCSV();
		ArrayList<Question> questionList = Pipeline.createQuestionsFromFiles(QUESTIONS_FILE_PATH, 
				TYPE_FILE_PATH, LIST_FILE_PATH);
		writeObject(questionList, "QuestionListTwo.bin");

//		ArrayList<Question> questionList = (ArrayList<Question>) readObject("QuestionList.bin");
		BufferedWriter writer;
		try {
			writer = new BufferedWriter(new FileWriter(new File(OUTPUT_PATH)));
		} catch(Exception ioe) {
			ioe.printStackTrace();
			return;
		}
		
		
		//for(Question question : questionList) {
		for(int x = firstQuestion; x < lastQuestion; x++) {
			Question question = questionList.get(x);
//			Question question = questionList.get(questionNum);
//			PodsGeneration.readCSV();
//			question.podLists.set(0, PodsGeneration.create_pods(question));
//			
			System.out.println(x);
			ArrayList<String> answerList  = new ArrayList<>();
			try {
				answerList = Pipeline.answerQuestion(question);
				for(String answer : answerList) {
					System.out.println(answer);
				}
			} catch(Exception e) {
				answerList.add("Error occurred when answering the question.");
			}
			try {
				
				writer.write(x + " : " + question.text + " : " + formatAnswers(answerList) + "\n");
				writer.flush();
			} catch(IOException ioe) {
				System.err.println("Error writing to file.");
			}
		}
		try {
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static String formatAnswers(ArrayList<String> answers) {
		if(answers.size() == 0 || (answers.size() == 1 && answers.get(0).equals(""))) {
			return "No answer.";
		}
		
		String answerString = "";
		for(int x = 0; x < answers.size() - 1; x++) {
			answerString = answerString + answers.get(x) + ",\n "+x;
		}
		answerString = answerString + answers.get(answers.size() - 1);
		return answerString;
	}
	
	public static void writeObject(Object object, String filepath) {
		try {
			FileOutputStream fos = new FileOutputStream(new File(filepath));
			ObjectOutputStream outputStream = new ObjectOutputStream(fos);
			outputStream.writeObject(object);
			outputStream.close();
			fos.close();	
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	//reads an object from a file
	public static Object readObject(String filepath) {
		Object obj = null;
		try {
			FileInputStream fis = new FileInputStream(new File(filepath));
			ObjectInputStream ois = new ObjectInputStream(fis);
			obj = ois.readObject();
			ois.close();
			fis.close();
		} catch(Exception e) {
			e.printStackTrace();
		}
		return obj;
	}
	
}
