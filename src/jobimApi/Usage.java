package jobimApi;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import java.net.URLEncoder;

import javax.json.Json;
import javax.json.stream.JsonParser;

public class Usage {
	public static void main(String[] args) {

		//		System.out.println(getSimilarityScore("joy","happiness"));
		try {
			String term1="";
			//String term2="";
			//System.out.println("Please enter the 2 terms whose similarity you want to calculate: ");
			System.out.println("Please enter the term:");
			BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

			term1 = br.readLine();
			//	term2 = br.readLine();
			HashMap tl = new HashMap<String, Integer>();
			tl = getSimilarTerms(term1,"NN");
			HashMap sortedtl = new HashMap<String, Integer>();
			sortedtl=(HashMap) sortByComparator(tl,false);

			//ArrayList<String> s = (ArrayList<String>) sortedtl.entrySet();

			//printMap(sortedtl);


						ArrayList<String> s = new ArrayList<String>();
						s = getSimilarTermsList(term1, "NN");
			//			// Get top n terms

			//			for (Map.Entry<String, Integer> entry : sortedtl.entrySet()) {
			//			    List<String> list = entry.getValue();
			//			    // Do things with the list
			//			}

			ArrayList<String> topFew = new ArrayList<String>();
			int n=10;
			for(int i=0; i< n ; i++){
				topFew.add(topFew.size(),s.get(i));
			}
			System.out.println(topFew);


		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * Returns a list of the strings, sorted in descending order by their scores
	 */

	public static ArrayList<String> sortedKeywords(Map<String,Integer> map) {
		List<Entry<String, Integer>> list = new LinkedList<Entry<String, Integer>>(map.entrySet());

		Collections.sort(list, new Comparator<Entry<String, Integer>>()
				{
			public int compare(Entry<String, Integer> o1, Entry<String, Integer> o2) {

				{
					return o2.getValue().compareTo(o1.getValue());

				}
			}
				});

		ArrayList<String> sortedWords = new ArrayList<String>();
		for (Entry<String,Integer> entry : list) {
			sortedWords.add(entry.getKey());
		}
		return sortedWords;
	}

	public static ArrayList<String> getSimilarTermsList(String term, String suffix){
		ArrayList<String> sim = new ArrayList<String>();
		
		Client c = new Client("http://maggie.lt.informatik.tu-darmstadt.de:10080/jobim/ws");
		try {
			term = URLEncoder.encode(term, "utf-8");
		} catch(Exception e) {
			e.printStackTrace();
		}
		String responseString = "/api/stanford/jo/similar/"+term+"%23"+suffix;
		
		System.out.println(responseString);
		Response r = c.get(responseString);
		//        Response r = c.get("/holing/stanford?s=Which+river+does+the+Brooklyn+Bridge+cross");
		//        Response r = c.get("/api/stanford/jo/bim/score/cross/Which+river+does+the+Brooklyn+Bridge+cross");
		//        Response r = c.get("/api/{holingtype}/jo/similar-score/{term1}/{term2}");
		r.show();
		String b = r.getBody();
		System.out.println(b);
		JsonParser parser = Json.createParser(new StringReader(b));

		String k = ""; 

		while (parser.hasNext()) {

			JsonParser.Event event = parser.next();


			switch(event) {
			case START_ARRAY:
			case END_ARRAY:
			case START_OBJECT:
				k="";
				break;
			case END_OBJECT:
			case VALUE_FALSE:
			case VALUE_NULL:
			case VALUE_TRUE:
				//System.out.println(event.toString());
				break;
			case KEY_NAME:
				//System.out.print(event.toString() + " " +	parser.getString() + " - ");
				break;
			case VALUE_STRING:
				k = parser.getString();
				//the strings have stuff at the end that we don't want
				if(k.contains("#")) {
					k = k.substring(0,k.indexOf('#'));
				}




				break;
			case VALUE_NUMBER:
				//				System.out.println(event.toString() + " " +
				//						parser.getString());
				break;
			}

			//System.out.println("k = "+k+"\tv = "+v);
			if(!k.equals("") && !k.equalsIgnoreCase("stanford") && !k.equalsIgnoreCase("getsimilarterms") && !sim.contains(k)){
				sim.add(sim.size(), k);
			}
		}




		return sim;
	}


	/**
	 * Returns a HashMap of similar terms with their similarity scores.
	 * @param term	: The term we need to find similar terms for.
	 * @return		: Pairs of (similarTerm, SimilarityScore).
	 */
	public static HashMap<String,Integer> getSimilarTerms(String term, String suffix){

		HashMap<String,Integer> similarTerms = new HashMap<String,Integer>();


		Client c = new Client("http://maggie.lt.informatik.tu-darmstadt.de:10080/jobim/ws");
		try {
			term = URLEncoder.encode(term, "utf-8");
		} catch(Exception e) {
			e.printStackTrace();
		}
		String responseString = "/api/stanford/jo/similar/"+term+"%23"+suffix;
		System.out.println(responseString);
		Response r = c.get(responseString);
		//        Response r = c.get("/holing/stanford?s=Which+river+does+the+Brooklyn+Bridge+cross");
		//        Response r = c.get("/api/stanford/jo/bim/score/cross/Which+river+does+the+Brooklyn+Bridge+cross");
		//        Response r = c.get("/api/{holingtype}/jo/similar-score/{term1}/{term2}");
		r.show();
		String b = r.getBody();
		System.out.println(b);
		JsonParser parser = Json.createParser(new StringReader(b));

		String k = ""; 
		Integer v = 0;

		while (parser.hasNext()) {

			JsonParser.Event event = parser.next();


			switch(event) {
			case START_ARRAY:
			case END_ARRAY:
			case START_OBJECT:
				k="";
				v=0;
				break;
			case END_OBJECT:
			case VALUE_FALSE:
			case VALUE_NULL:
			case VALUE_TRUE:
				//System.out.println(event.toString());
				break;
			case KEY_NAME:
				//System.out.print(event.toString() + " " +	parser.getString() + " - ");
				break;
			case VALUE_STRING:
				k = parser.getString();
				//the strings have stuff at the end that we don't want
				if(k.contains("#")) {
					k = k.substring(0,k.indexOf('#'));
				}
				break;
			case VALUE_NUMBER:
				//				System.out.println(event.toString() + " " +
				//						parser.getString());
				v = (parser.getInt());
				break;
			}

			//System.out.println("k = "+k+"\tv = "+v);
			if(!k.equals("") && !v.equals("")){
				similarTerms.put(k, v);
			}

		}
		return similarTerms;
	}

	/**
	 * 
	 * Sorting the hash map
	 * @param unsortMap
	 * @param order
	 * @return
	 */
	private static Map<String, Integer> sortByComparator(Map<String, Integer> unsortMap, final boolean order)
	{

		List<Entry<String, Integer>> list = new LinkedList<Entry<String, Integer>>(unsortMap.entrySet());

		// Sorting the list based on values
		Collections.sort(list, new Comparator<Entry<String, Integer>>()
				{
			public int compare(Entry<String, Integer> o1,
					Entry<String, Integer> o2)
			{
				if (order)
				{
					return o1.getValue().compareTo(o2.getValue());
				}
				else
				{
					return o2.getValue().compareTo(o1.getValue());

				}
			}
				});

		// Maintaining insertion order with the help of LinkedList
		Map<String, Integer> sortedMap = new LinkedHashMap<String, Integer>();
		for (Entry<String, Integer> entry : list)
		{
			sortedMap.put(entry.getKey(), entry.getValue());
		}

		return sortedMap;
	}

	public static void printMap(Map<String, Integer> map)
	{
		for (Entry<String, Integer> entry : map.entrySet())
		{
			System.out.println("  " + entry.getKey() + "\t : "+ entry.getValue());
		}
	}
	/*
	 * Returns a score indicating the similarity between the 2 terms.
	 * A high value indicicates a high similarity.
	 */
	public static float getSimilarityScore(String term1, String term2, String suffix1, String suffix2){

		float score=0f;
		Client c = new Client("http://maggie.lt.informatik.tu-darmstadt.de:10080/jobim/ws");
		try {
			term1 = URLEncoder.encode(term1, "utf-8");
			term2 = URLEncoder.encode(term2, "utf-8");
		} catch(Exception e) {
			e.printStackTrace();
		}
		String responseString = "/api/stanford/jo/similar-score/"+term1+ "#" + suffix1 + "/"+term2 + "#" + suffix2;
		Response r = c.get(responseString);
		r.show();
		String b = r.getBody();
		System.out.println(b);
		JsonParser parser = Json.createParser(new StringReader(b));

		while (parser.hasNext()) {
			JsonParser.Event event = parser.next();
			switch(event) {
			case START_ARRAY:
			case END_ARRAY:
			case START_OBJECT:
			case END_OBJECT:
			case VALUE_FALSE:
			case VALUE_NULL:
			case VALUE_TRUE:
				System.out.println(event.toString());
				break;
			case KEY_NAME:
				System.out.print(event.toString() + " " +
						parser.getString() + " - ");
				break;
			case VALUE_STRING:
			case VALUE_NUMBER:
				System.out.println(event.toString() + " " +
						parser.getString());
				break;
			}
		}
		return score;
	}





}

