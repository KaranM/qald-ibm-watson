package jobimApi;

public class Response {
	public int statusCode;
	public String body = "";
	public String exception="";
	
	public void show(){
		
		System.out.println("Status code:"+statusCode);
		System.out.println("Body : "+body);
		System.out.println("Exception : "+exception);
		
	}

	public int getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public String getException() {
		return exception;
	}

	public void setException(String exception) {
		this.exception = exception;
	}
	
	
	
}
