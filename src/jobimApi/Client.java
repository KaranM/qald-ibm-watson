package jobimApi;

	import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

	public class Client {
		private String _baseUrl;

		public Client(String url) 
		{
			_baseUrl = url;		
		}

		public Response get(String uri)
		{
			return this.execute(uri, Method.GET, null);
		}

		public Response post(String uri, String body)
		{
			return this.execute(uri, Method.POST, body);
		}

		public Response execute(String uri, Method method, String body) 
		{
			Response response = new Response();

			try 
			{
				URL url = new URL(_baseUrl + uri);
				HttpURLConnection conn = (HttpURLConnection) url.openConnection();

				conn.setInstanceFollowRedirects(false); 
				conn.setRequestMethod(method.toString()); 
				conn.setRequestProperty("Content-Type", "application/json"); 

		        if (method == Method.POST ) 
		        {
					conn.setDoOutput(true); 
		        	final OutputStream os = conn.getOutputStream();
	                os.write(body.getBytes());
	                os.flush();
	                os.close();
		        }

		        InputStream is = conn.getInputStream(); 
		        BufferedReader rd = new BufferedReader(new InputStreamReader( is));

		        String line;
		        while ((line = rd.readLine()) != null) 
		        {
		            response.body += line;
		        }	        
		        rd.close();

		        response.statusCode = conn.getResponseCode(); 
		        conn.disconnect(); 
			} 
			catch (Exception e) 
			{
				response.exception = e.getMessage();
			}
	        return response;
		}
}
