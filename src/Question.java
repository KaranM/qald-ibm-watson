import java.util.ArrayList;
import java.io.Serializable;

public class Question implements Serializable {

	String text;
	boolean isList;
	QuestionType questionType;
	
	ArrayList<ArrayList<PODNode>> podLists;
	
	public void setType(String type) {
		type = type.toLowerCase();
		switch(type) {
		case "place":
			this.questionType = QuestionType.PLACE;
			return;
		case "person":
			this.questionType = QuestionType.PERSON;
			return;
		case "number":
			this.questionType = QuestionType.NUMBER;
			return;
		case "date":
			this.questionType = QuestionType.DATE;
			return;
		case "boolean":
			this.questionType = QuestionType.BOOLEAN;
			return;
		default:
			this.questionType = QuestionType.OBJECT;
			return;
		}
	}
	
	public void setIsList(String isList) {
		this.isList = (isList.equals("yes") ? true : false);
	}

	public Question()
	{
		podLists = new ArrayList<ArrayList<PODNode>>();
	}
	
}

