

public class Triple {
	String subject;
	String predicate;
	String object;
	
	boolean subjectLiteral;
	boolean predicateLiteral;
	boolean objectLiteral;
	
	public Triple(String sub, String pred, String obj) {
		this.subject = sub;
		this.predicate = pred;
		this.object = obj;
		
		this.subjectLiteral = false;
		this.predicateLiteral = false;
		this.objectLiteral = false;
	}
	
	public Triple(String sub, String pred, String obj, boolean subLit, boolean predLit, boolean obLit) {
		this.subject = sub;
		this.predicate = pred;
		this.object = obj;
		
		this.subjectLiteral = subLit;
		this.predicateLiteral = predLit;
		this.objectLiteral = obLit;	
	}
}
