import java.io.IOException;
import java.io.InputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;

import java.net.URLConnection;
import java.net.URL;
import java.net.URLEncoder;

import java.util.regex.*;
import java.util.ArrayList;

public class DBPediaInterface {
	
	//returns a csv list of rows in response to the query over dbpedia
	public static String queryDBPedia(String query) {
		String result = "";
		
		query = cleanURLString(query);
		System.out.println(query);
		String url = "http://dbpedia.org/sparql?default-graph-uri=http%3A%2F%2Fdbpedia.org&query=" + query + "&format=csv";
		
		//connect to the api and read the resulting html page
		URLConnection conn;
		InputStream response;
		try {
			conn = new URL(url).openConnection();
			conn.setRequestProperty("Accept-Charset","UTF-8");
			response = conn.getInputStream();
			BufferedReader reader = new BufferedReader(new InputStreamReader(response));
			String line;
			while((line = reader.readLine()) != null) {
				line = line + "\n";
				result += line;
			}
		} catch(IOException ioe) {
			ioe.printStackTrace();
			return null;
		}
		return result;
		
	}
	
	/**
	 * Returns the pivot keyword from a list of keywords
	 * 
	 * The pivot is determined based on which keyword corresponds to the dbpedia resource
	 * with the most edges in the graph
	 * 
	 * @param keywords
	 * @return
	 */
	public static Word getPivot(ArrayList<Word> keywords) {
		Word bestKeyword = null;
		int mostRelations = 0;
		for(Word keyword : keywords){
			//only use best keyword result
			String resource = getResourceURLs(keyword.text).get(0);
				try {
				//query returns the number of edges coming out of the resource
				String query = "SELECT COUNT(?pred) WHERE {{"
						+ "?x ?pred dbpedia:" + shortenResource(resource)
						+ "} UNION {dbpedia:" + shortenResource(resource) + "?pred ?x}}";
				int numRelations = Integer.parseInt(queryDBPedia(query).split("\n")[1]);
			
				if(numRelations > mostRelations) {
					mostRelations = numRelations;
					bestKeyword = keyword;
					}
				}
				//gracefully continue if there's an exception
				catch(Exception e) {
					e.printStackTrace();
					continue;
				}
			}
		
		return bestKeyword;
	}
	
	public static ArrayList<String> queryLocation(String fullResource) {
		ArrayList<String> results = new ArrayList<String>();
		
		String shortenedResource = fullResource.substring("http://dbpedia.org/resource/".length());
		String query = "SELECT DISTINCT ?location WHERE {"
				+ "{dbpedia:" + shortenedResource + " dbpedia-owl:city ?location} UNION"
				+ "{dbpedia:" + shortenedResource + " dbpedia-owl:location ?location} UNION"
				+ "{dbpedia:" + shortenedResource + " dbpedia-owl:country ?location} UNION"
				+ "{dbpedia:" + shortenedResource + " dbpprop:location ?location} UNION" 
				+ "{dbpedia:" + shortenedResource + " dbpedia-owl:locatedInArea ?location}"
				+ "}";
		
		String response = queryDBPedia(query);
		
		//each line contains one resource
		String[] lines = response.split("\n");
		
		//ignore the first line
		for(int x = 1; x < lines.length; x++) {
			//remove quotation marks if there are any
			String line = lines[x];
			if(line.charAt(0) == '"') {
					line = line.substring(1, line.length() - 1);
			}
			results.add(line);
		}
		
		return results;
	}
	
	//filter a list of resources by the resources that have the given properties
	//the resource can have any relation to those properties
	public static ArrayList<String> filterResults(ArrayList<String> startingResources, ArrayList<String> attributeResources) {
		
		ArrayList<String> subset = new ArrayList<String>();
		
		//query based on the restrictions, filter based on the starting resources
		String query = "SELECT DISTINCT ?x WHERE {";
		for (int x = 0; x < attributeResources.size(); x++) {
			query += "{?x ?y dbpedia:" + shortenResource(attributeResources.get(x)) + "}";
			if(x < attributeResources.size() - 1) {
				query += " UNION ";
			}
		}
		query += "FILTER(";
		for(int x = 0; x < startingResources.size(); x++) {
			query += "?x = dbpedia:" + shortenResource(startingResources.get(x));
			if(x < startingResources.size() - 1) {
				query += " || ";
			}
		}
		query += ")}";
		
		//finish up the query
		String response = queryDBPedia(query);
		String[] responsePieces = response.split("\n");
		//skip the header line
		for(int x = 1; x< responsePieces.length; x++) {
			//get rid of quotation marks if necessary
			String result = responsePieces[x];
			if(result.charAt(0) == '"') {
				result= result.substring(1,result.length() - 1);
			}
			subset.add(result);
		}
		return subset;
	}
	
	//returns an ArrayList of subject-predicate-object pairs for the given DBPedia Resource
	public static ArrayList<Triple> getRelationships(String fullResource) {
		ArrayList<Triple> relationships = new ArrayList<Triple>();
		
		//get the suffix of the resource for ease of use
		//String shortenedResource = fullResource.substring("http://dbpedia.org/resource/".length());
		String shortenedResource = shortenResource(fullResource);
		shortenedResource = shortenedResource.replace(' ', '_');
		shortenedResource = shortenedResource.replace(',','_');
		
		//first get relationships where the resource is the subject
		String query = String.format("SELECT dbpedia:%s ?pred ?obj WHERE {dbpedia:%s ?pred ?obj}",shortenedResource, shortenedResource);
		
		String csvResponse = queryDBPedia(query);
		String[] responseLines = csvResponse.split("\n");
		
		//first line is just the header, we can ignore it
		for (int x = 1; x < responseLines.length; x++) {
			//there could be an error if response is oddly formatted
			try {
				String line = responseLines[x];
				relationships.add(parseCSVLine(line));
			} catch(Exception e) {
				e.printStackTrace();
			}
		}
		
		//then get relationships where the resource is the object
		query = String.format("SELECT ?subj ?pred dbpedia:%s WHERE {?subj ?pred dbpedia:%s}",shortenedResource, shortenedResource);
		
		csvResponse = queryDBPedia(query);
		responseLines = csvResponse.split("\n");
		
		//first line is just the header, we can ignore it
		for (int x = 1; x < responseLines.length; x++) {
			String line = responseLines[x];
			relationships.add(parseCSVLine(line));
		}
		
		
		return relationships;
	}
	
	//returns an arraylist of possible dbpedia resources for the keyword
	public static ArrayList<String> getResourceURLs(String keyword) {
		
		ArrayList<String> resourceURLs = new ArrayList<String>(); 
		
		//resourcesXML is an XML file containing a list of possible resources that match the keyword
		String resourcesXML = "";
		try {
			resourcesXML = getPossibleResources(URLEncoder.encode(keyword, "utf-8"));
		} catch(Exception e) {
			System.err.println("Could not get resource for keyword");
			e.printStackTrace();
		}
		
		//even though searching XML files with regexes is evil, it works well in this case
		Pattern pattern = Pattern.compile("<Result>.*?<URI>(.*?)</URI>.*?</Result>");
		Matcher matcher = pattern.matcher(resourcesXML);
		while(matcher.find()) {
			String url = matcher.group(1);
			resourceURLs.add(url);
		}
		
		return resourceURLs;
	}
	
	private static String getPossibleResources(String keyword) {
		String result = "";
		//clean up the keyword so it's a valid URL
		keyword = cleanURLString(keyword);
		
		String url = "http://lookup.dbpedia.org/api/search.asmx/KeywordSearch?QueryString=" + keyword;
		
		//connect to the api and read the resulting html page
		URLConnection conn;
		InputStream response;
		try {
			conn = new URL(url).openConnection();
			conn.setRequestProperty("Accept-Charset","UTF-8");
			response = conn.getInputStream();
			BufferedReader reader = new BufferedReader(new InputStreamReader(response));
			String line;
			while((line = reader.readLine()) != null) {
				//don't include newlines to make regex search easier
				result += line;
			}
		} catch(IOException ioe) {
			ioe.printStackTrace();
			return null;
		}
		return result;
	}
	
	public static String cleanURLString(String urlString) {
		try {
			return URLEncoder.encode(urlString, "utf-8");
		} catch(Exception e) {
			return null;
		}
	}
	
	/*
	 * Function to parse a line of a csv response into an RDF triple
	 * Assume that the format is subject, predicate, object
	 * Sometimes the terms will have quotation marks, sometimes not
	 */
	private static Triple parseCSVLine(String CSVLine) {
		Pattern pattern = Pattern.compile("([^\"]+|\"[^\"]+\"),([^\"]+|\"[^\"]+\"),([^\"]+|\"[^\"]+\")");
		Matcher matcher = pattern.matcher(CSVLine);
		matcher.find();
		String subj = matcher.group(1);
		String pred = matcher.group(2);
		String obj = matcher.group(3);
		
		//if the rdf entity starts with ", then it's a resource
		boolean subLiteral = (subj.charAt(0) == '"' ? false : true);
		boolean predLiteral = (pred.charAt(0) == '"' ? false : true);
		boolean objLiteral = (obj.charAt(0) == '"' ? false : true);
		
		//we don't want to store the extra quotation marks
		if(!subLiteral) {
			subj = subj.substring(1, subj.length() - 1);
		}
		if (!predLiteral) {
			pred = pred.substring(1, pred.length() - 1);
		}
		if (!objLiteral) {
			obj = obj.substring(1, obj.length() - 1);
		}
		
		return new Triple(subj, pred, obj, subLiteral, predLiteral, objLiteral);

	}
	
	//remove the prefix from a resource
	public static String shortenResource(String resource) {
		//get the location of the last slash or colon in the resource
		int index = resource.lastIndexOf('/');
		if(index == -1) {
			index = resource.lastIndexOf(':');
		}
		resource = resource.substring(index+1);
		
		//deal with the camel case
		//resource = splitCamelCase(resource);
		
		return resource;
		
	}
	
	//code taken from stack overflow
	static String splitCamelCase(String s) {
		   return s.replaceAll(
		      String.format("%s",
		         "(?<=[A-Z])(?=[A-Z][a-z])"
		      ),
		      " "
		   );
		}
	
}
