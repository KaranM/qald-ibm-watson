import pdb
import sys
import xml.etree.ElementTree as ET

def format_question(question):
    '''all questions start with ![CDATA[ and end with  ]], remove them'''
    return question

def get_english_question(question):
    '''return a list of answers from the question node'''
    for child in question:
	if child.tag == 'string' and child.attrib.get('lang',None) == 'en':
	    return format_question(child.text)
    #avoid breaking program - always return something
    return ''

#read the xml file from standard input
answers_xml = sys.stdin.read()
root = ET.fromstring(answers_xml)

for question in root:
    print get_english_question(question)
