import urllib
import sys
import pdb
from bs4 import BeautifulSoup

def search_url(question):
    '''return the url that does a search of wikipedia'''
    encoded_question = urllib.quote(question)
    return 'https://en.wikipedia.org/w/index.php?search={0}'.format(encoded_question)

def get_first_result(html_string):
    '''returns the first wikipedia result from the search page's html'''
    soup = BeautifulSoup(html_string)
    url = soup.find_all('div',{'class':'mw-search-result-heading'})[0]\
        .a.get('href')
    if('en.wikipedia.org/wiki/' not in url):
        url = 'en.wikipedia.org/wiki/' + url
    return url

#    url = soup.findall('.mw-search-result-heading')[0].findall('a')[0]

def query_wikipedia(question):
    '''returns a wikipedia resource by searching a question'''
    search_result = urllib.urlopen(search_url(question))
    first_url = search_result.geturl()

    #if it redirects to a resource, return that
    if('en.wikipedia.org/wiki/' in first_url):
        return first_url

    #otherwise, return the first link of the results
    return get_first_result(search_result.read())

def get_baseline():
    question_list = sys.stdin.readlines()
    for question in question_list:
        if(question[-1] == '\n'):
            question = question[:-1]
        print(query_wikipedia(question))

if __name__ == '__main__':
    get_baseline()
